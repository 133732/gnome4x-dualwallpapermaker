#!/bin/bash

echo "please give a name"
read BGNAME
PICLIGHT=`zenity --file-selection --title="Choose the light background"`
echo "you chose" $PICLIGHT
PICDARK=`zenity --file-selection --title="Choose the dark background"`
echo "you chose $PICDARK"

mkdir -p ~/.local/share/gnome-background-properties && cd ~/.local/share/gnome-background-properties
touch $BGNAME.xml

echo "<?xml version=\"1.0\"?>
<!DOCTYPE wallpapers SYSTEM \"gnome-wp-list.dtd\">
<wallpapers>
  <wallpaper deleted=\"false\">
    <name>Default Background</name>
    <filename>$PICLIGHT</filename>
    <filename-dark>$PICDARK</filename-dark>
    <options>zoom</options>
    <shade_type>solid</shade_type>
    <pcolor>#3465a4</pcolor>
    <scolor>#000000</scolor>
  </wallpaper>
</wallpapers>" > $BGNAME.xml
