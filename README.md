# gnome4x-dualwallpapermaker

a small script to create wallpapers that adapt to light or dark mode

## Getting started

1. Download the script:
`git clone https://gitlab.com/tecbuddy/gnome4x-dualwallpapermaker.git`

2. Change the directory 
`cd gnome4x-dualwallpapermaker/`

3. Make script executable:
`sudo chmod u+x dualwallpaper-maker.sh`

4. execute script:
`./dualwallpaper-maker.sh`

The script will ask you to choose a name and two wallpapers. First the light, then the dark.
After that it creates the corresponding XML file in: `~/.local/share/gnome-background-properties`
